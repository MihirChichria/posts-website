import http from './slhttp';
const url = window.location.href;
const author_id = url.substring(url.length - 1, url.length);
if(!(window.location.href == "http://localhost:8081/")){
    init();
    const name = document.getElementById("name").onmouseover = showDetails;
    const email = document.getElementById("email").onmouseover = showDetails;
    const birthday = document.getElementById("birthday").onmouseover = showDetails;
    const address = document.getElementById("address").onmouseover = showDetails;
    const phone = document.getElementById("phone").onmouseover = showDetails;
    const password = document.getElementById("password").onmouseover = showDetails;

    const author_title = document.getElementById("author-title");
    const author_value = document.getElementById("author-value");

async function init(){
    await http.get(`http://localhost:3000/authorInfo/${author_id}`)
    .then(author => {
        document.getElementById("author-img").src = "images/"+author.authorImg;
        document.getElementById("name").dataset.value = author.authorName;
        document.getElementById("email").dataset.value = author.authorEmail;
        document.getElementById("birthday").dataset.value = author.authorBDate;
        document.getElementById("address").dataset.value = author.authorAddress;
        document.getElementById("phone").dataset.value = author.authorPhone;
        document.getElementById("password").dataset.value = author.authorPassword;
    })
    .catch(err => console.log(err))
    
    document.getElementById("author-title").innerHTML = document.getElementById("name").dataset.id;
    document.getElementById("author-value").innerHTML = document.getElementById("name").dataset.value;
}
function showDetails(){
    
    author_title.innerHTML = this.dataset.id;
    const that = this;
    http.get(`http://localhost:3000/authorInfo/${author_id}`)
        .then(author => {
            const value = that.dataset.value;
            author_value.innerHTML = value;
        })
}
}