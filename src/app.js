import http from './slhttp';
import ui from './ui';

//EVENTS
if(window.location.href == 'http://localhost:8081/'){
//    console.log("inside!");
    document.addEventListener('DOMContentLoaded', fetchPosts);
    document.getElementById('add_post_btn').addEventListener('click', addPost);
    document.getElementById('posts').addEventListener('click', deletePost);
    document.getElementById('posts').addEventListener('click', editPost);
    document.getElementById('posts').addEventListener('click', fetchAuthorInfo);
    document.getElementById('edit_post_btn').addEventListener('click', updatePostData);
}
//ACTUAL FUNCTIONS
function fetchPosts(){
    http.get("http://localhost:3000/posts")
        .then(data => ui.showPosts(data))
        .catch(err => console.log(err));
}

function addPost(){
    const title     =   document.getElementById('post_title').value;
    const body      =   document.getElementById('post_body').value;
    const author    =   document.getElementById('post_author').value;

    const data ={
        title,
        author,
        body
    };

    http.post("http://localhost:3000/posts", data)
        .then(data => {
            ui.showAlert('Post Created Successfully!', 'alert alert-success');
            ui.clearFields();
            fetchPosts();
        })
        .catch(err =>console.error(err));
}

function deletePost(e){
    e.preventDefault();
    // console.log(e.target);
    if(e.target.classList.contains('delete')){
        const id = e.target.dataset.id;
        // console.log(id);
        if(confirm("Are you sure you want to delete the post?")){
            http.delete(`http://localhost:3000/posts/${id}`)
                .then(data => {
                    ui.showAlert("Post Deleted Successfully!", "alert alert-success");
                    fetchPosts();
                })
                .catch(err => console.log(err));

        }
    }
}

function editPost(e){
    e.preventDefault();
    if(e.target.classList.contains('edit')){
        const id = e.target.dataset.id;
        http.get(`http://localhost:3000/posts/${id}`)
            .then(post => ui.fillModalData(post))
            .catch(err => console.log(err));
    }
}

function fetchAuthorInfo(e){
    e.preventDefault();
    if(e.target.classList.contains('author')){
//        console.log('Author!');
        const id = e.target.dataset.id;
        http.get(`http://localhost:3000/authorInfo/${id}`)
            .then(authorInfo => ui.showAuthorDetails(authorInfo))
            .catch(err => console.log(err));
    }
}

function updatePostData(){
    const title     =   document.getElementById('edit_post_title').value;
    const body      =   document.getElementById('edit_post_body').value;
    const author    =   document.getElementById('edit_post_author').value;
    const id        =   document.getElementById('edit_post_id').value;

    const data ={
        title,
        author,
        body
    };

    http.put(`http://localhost:3000/posts/${id}`, data)
        .then(data => {
            ui.showAlert('Post Edited Successfully!', 'alert alert-success');
            fetchPosts();
        })
        .catch(err =>console.error(err));

    //CLOSING THE MODAL
    $("#editPost").modal('hide');
}
